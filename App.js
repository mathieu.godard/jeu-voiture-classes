import { Voiture } from "./Voiture.js";

const Voiture1 = new Voiture ('Peugeot', '206', 130);
const Voiture2 = new Voiture ('Renault', 'Twingo', 150);
const Voiture3 = new Voiture ('Ferrari', 'F355', 200);
const Voiture4 = new Voiture ('Audi', 'R8', 190);
const Voiture5 = new Voiture ('Tesla', 'T55', 90);
const Voiture6 = new Voiture ('Citroën', 'C1', 80);

const voitures = [
    Voiture1, 
    Voiture2, 
    Voiture3, 
    Voiture4, 
    Voiture5, 
    Voiture6
]

function tirerAuSortVoiture(voitures) {
    const random = Math.floor(Math.random() * voitures.length);
    const choosen = voitures.splice(random, 1)[0];
    return choosen;
}

const Car1 = tirerAuSortVoiture(voitures);
const Car2 = tirerAuSortVoiture(voitures);

Car1.demarrer();
Car2.demarrer();

const lancerJeu = setInterval(() => {
    Car1.accelerer();
    Car1.majDistanceParcourue();
    Car2.accelerer();
    Car2.majDistanceParcourue();
    if (Car1.distanceParcourue = Car2.distanceParcourue && Car1.distanceParcourue >= 1) {
        console.log(`Egalité parfaite !`);
        clearInterval(lancerJeu);
    }
    if (Car1.distanceParcourue > Car2.distanceParcourue && Car1.distanceParcourue >= 1) {
        console.log(`La ${Car1.modele} a gagné.`);
        clearInterval(lancerJeu);
    }
    if (Car2.distanceParcourue > Car1.distanceParcourue && Car2.distanceParcourue >= 1) {
        console.log(`La ${Car2.modele} a gagné.`);
        clearInterval(lancerJeu);
    }
}, 100);
