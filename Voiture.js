export class Voiture {

    distanceParcourue;
    vitesseInstantT = 0;

    constructor(marque, modele, vitesse) {
        this.marque = marque;
        this.modele = modele;
        this.vitesse = vitesse;
    }

    demarrer() {
        this.distanceParcourue = 0;
        console.log(`La ${this.modele} vient de démarrer.\nDistance parcourue : ${this.distanceParcourue} km.`);
    }

    accelerer() {
        if (this.vitesseInstantT < this.vitesse) {
            this.vitesseInstantT++;
            console.log(`Vitesse de la ${this.modele} : ${this.vitesseInstantT} km/h.`);
        } else {
            console.log(`Vitesse de la ${this.modele} : ${this.vitesseInstantT} km/h.`);;
        }
    }

    majDistanceParcourue() {
        this.distanceParcourue += this.vitesseInstantT / 10;
        console.log(`Distance parcourue de la ${this.modele} : ${this.distanceParcourue} km.\n`);
    }
}

